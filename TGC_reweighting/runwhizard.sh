#!/bin/bash
SAMPLE=$3
SUBSAMPLE=${SAMPLE}_$2
mkdir $SUBSAMPLE 
SINDARIN=$1
mv ${SINDARIN}.sin $SUBSAMPLE
cd $SUBSAMPLE
pwd  
# setup whizard
source /cvmfs/clicdp.cern.ch/software/WHIZARD/2.8.3-27bf09e/x86_64-slc6-gcc7-opt/setup.sh
eosfusebind
#run Whizard
whizard ${SINDARIN}.sin
cp whizard.log ../  
echo "ls "
ls  
rm *.evx #because it's huge and I only need the stdhep and the evt
echo "ls ../"
cat decay_proc.evt | grep -e "alternate weight" -e "Event \#" > ${SUBSAMPLE}_weights.txt
ls ../  
cd ../
tar -cvzf ${SUBSAMPLE}.tar.gz $SUBSAMPLE/decay_proc.stdhep $SUBSAMPLE/whizard.log $SUBSAMPLE/decay_proc.evt $SUBSAMPLE/${SUBSAMPLE}_weights.txt
cp ${SUBSAMPLE}.tar.gz /eos/user/s/schnooru/Data_CLIC//whizard_qqlnu_1.4tev/output_tarballs/${SAMPLE}



