##################################################
# e+e- -> qqll at 3 TeV
# ulrike.schnoor@cern.ch
##################################################

! Model and Process block
model        = SM_CKM

ms     = 0
mc     = 0
mb     = 0
mtop   = 174 GeV
wtop   = 1.37 GeV
mW     = 80.45 GeV
wW     = 2.071 GeV
mZ     = 91.188 GeV
wZ     = 2.478 GeV
mH     = 125 GeV
wH     = 0.00407 GeV

! remove overlap with higher multiplicities
alphas = 0

alias q = u:d:s:c:b
alias qbar = U:D:S:C:B

! Beam block
sqrts = 3000 GeV

#  resonance history
?resonance_history         = true
resonance_on_shell_limit   = 16
resonance_on_shell_turnoff = 8

alias uptype       = u:c
alias antiuptype   = U:C
alias downtype	   = d:s:b
alias antidowntype = D:S:B

process decay_proc = e1, E1 => ( e1, E1 , uptype, antiuptype )
 		     	     + ( e1, E1 , downtype, antidowntype )
                             + ( e2, E2 , uptype, antiuptype )
 		     	     + ( e2, E2 , downtype, antidowntype )
                             + ( e3, E3 , uptype, antiuptype )
 		     	     + ( e3, E3 , downtype, antidowntype )


! circe2 beam spectrum and ISR
beams              = e1, E1  => circe2 => isr
?keep_beams        = true
!isr_order         = 3
?isr_handler       = true
$isr_handler_mode = "recoil"
isr_alpha          = 0.0072993
isr_mass           = 0.000511
beams_pol_density  = @(+1), @()
beams_pol_fraction = 80%, 0%

$circe2_file = "/cvmfs/clicdp.cern.ch/software/WHIZARD/circe_files/CLIC/3TeVeeMapPB0.67E0.0Mi0.15.circe"
$circe2_design = "CLIC"
?circe2_polarized = false



! Cuts block
real default_M_cut = 4 GeV
real default_jet_cut = 10 GeV
real default_Q_cut = 4 GeV
alias quarks = u:d:s:c:b:U:D:S:C:B
alias lepton = e1:E1:e2:E2:e3:E3
cuts = all M > default_M_cut [lepton,lepton]
       and all M > default_jet_cut [quarks,quarks]
       and all M < - default_Q_cut [incoming e1, e1]
       and all M < - default_Q_cut [incoming E1, E1]
! Parton shower and hadronization

?ps_fsr_active		= true
?ps_isr_active		= false
?hadronization_active	= true
$shower_method		= "PYTHIA6"
!?ps_PYTHIA_verbose	= true


$ps_PYTHIA_PYGIVE = "MSTJ(28)=0; PMAS(25,1)=120.; PMAS(25,2)=0.3605E-02; MSTJ(41)=2; MSTU(22)=2000; PARJ(21)=0.40000; PARJ(41)=0.11000; PARJ(42)=0.52000; PARJ(81)=0.25000; PARJ(82)=1.90000; MSTJ(11)=3; PARJ(54)=-0.03100; PARJ(55)=-0.00200; PARJ(1)=0.08500; PARJ(3)=0.45000; PARJ(4)=0.02500; PARJ(2)=0.31000; PARJ(11)=0.60000; PARJ(12)=0.40000; PARJ(13)=0.72000; PARJ(14)=0.43000; PARJ(15)=0.08000; PARJ(16)=0.08000; PARJ(17)=0.17000; MSTP(3)=1;MSTP(71)=1"


integrate (decay_proc) {iterations = 10:100000:"gw", 7:10000:""}
show(results)
